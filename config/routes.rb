Rails.application.routes.draw do
  resources :posts
  resources :projects
  get 'user/index'

  get 'user/show'

  get 'user/edit'

  get 'user/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
